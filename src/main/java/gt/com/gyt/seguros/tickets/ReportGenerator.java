package gt.com.gyt.seguros.tickets;

import com.google.api.services.drive.Drive;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Thread;
import gt.com.gyt.seguros.tickets.util.CsvUtil;
import gt.com.gyt.seguros.tickets.util.DriveUtil;
import gt.com.gyt.seguros.tickets.util.GmailUtil;
import gt.com.gyt.seguros.tickets.util.ServiceUtil;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author descolin
 */
public class ReportGenerator {

    private static final String DEFAULT_QUERY = "after:${initDate} AND before:${endDate}";
    private static final String FILE_ID = "1xV6JVCvoFRSJI18SE-bbJmqeps8ammo9tTt1kIOoEx8";
    private static final String DATE_PATTERN = "yyyy/MM/dd";

    public static void main(String[] args) {
        LocalDateTime init = LocalDateTime.now();
        System.out.println("start " + init.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
        Gmail gmailService = ServiceUtil.getGmailService();
        Drive driveService = ServiceUtil.getDriveService();
        List<Map<String, String>> messageDetails = new ArrayList<>();
        List<Thread> threads = new ArrayList<>();
        try {
            LocalDate initDate = LocalDate.now().minusDays(30);
            LocalDate endDate = LocalDate.now();
            String strInitDate = initDate.format(DateTimeFormatter.ofPattern(DATE_PATTERN));
            String strEndDate = endDate.format(DateTimeFormatter.ofPattern(DATE_PATTERN));
            threads = GmailUtil.getThreadsByQuery(gmailService, "me",
                    DEFAULT_QUERY.replace("${initDate}", strInitDate).replace("${endDate}", strEndDate));
            messageDetails = listFiles(threads, gmailService);
            CsvUtil.createCSVFile(messageDetails);
            DriveUtil.updateFile(driveService, FILE_ID, "Tickets", "application/vnd.google-apps.spreadsheet", "tickets.csv");
        } catch (IOException ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        LocalDateTime end = LocalDateTime.now();
        System.out.println("end " + end.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
    }

    public static List<Map<String, String>> listFiles(List<Thread> threads, Gmail gmailService) {
        List<Map<String, String>> result = new ArrayList<>();
        threads.forEach((thread) -> {
            try {
                Map<String, String> map = new HashMap<>();
                map.putAll(GmailUtil.getMessageDetailsByThreadId(thread.getId(), gmailService));
                result.add(map);
            } catch (GeneralSecurityException ex) {
                Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        return result;
    }
}
