package gt.com.gyt.seguros.tickets.util;

import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DriveUtil {

    private static final String FILE_ID = "1xV6JVCvoFRSJI18SE-bbJmqeps8ammo9tTt1kIOoEx8";

    /**
     * Upload a file.
     * 
     * @param service Drive API service instance.
     * @param name Name of the file.
     * @param mimeType MIME type of the file.
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public static void uploadFile(Drive service, String name, String mimeType) throws IOException, GeneralSecurityException {
        File fileMetadata = new File();
        fileMetadata.setName(name);
        fileMetadata.setMimeType(mimeType);
        java.io.File filePath = new java.io.File("tickets.csv");
        FileContent mediaContent = new FileContent("text/csv", filePath);
        File file = service.files().create(fileMetadata, mediaContent)
                .setFields("id")
                .execute();
        System.out.println("File ID: " + file.getId());

    }

    /**
     * Update an existing file's metadata and content.
     *
     * @param service Drive API service instance.
     * @param fileId ID of the file to update.
     * @param newTitle New title for the file.
     * @param newDescription New description for the file.
     * @param newMimeType New MIME type for the file.
     * @param newFilename Filename of the new content to upload.
     * @param newRevision Whether or not to create a new revision for this file.
     * @return Updated file metadata if successful, {@code null} otherwise.
     */
    public static File updateFile(Drive service, String fileId,
            String newDescription, String newMimeType, String newFilename) {
        try {
            // First retrieve the file from the API.
            File file = service.files().get(fileId).execute();

            // File's new metadata.
            File fileObjectWithUpdates = new File();
            fileObjectWithUpdates.setDescription(newDescription);
            fileObjectWithUpdates.setMimeType(newMimeType);
            // File's new content.
            java.io.File fileContent = new java.io.File(newFilename);
            FileContent mediaContent = new FileContent(newMimeType, fileContent);
            // Send the request to the API.
            File updatedFile = service.files().update(file.getId(), fileObjectWithUpdates, mediaContent).execute();
            return updatedFile;
        } catch (IOException ex) {
            Logger.getLogger(DriveUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static void listFiles(Drive service) throws IOException {
        FileList result = service.files().list()
                .setPageSize(10)
                .setFields("nextPageToken, files(id, name)")
                .execute();
        List<File> files = result.getFiles();
        if (files == null || files.isEmpty()) {
            System.out.println("No files found.");
        } else {
            System.out.println("Files:");
            for (File file : files) {
                System.out.printf("%s (%s)\n", file.getName(), file.getId());
            }
        }
    }

//    public static void main(String... args) throws IOException, GeneralSecurityException {
//        // Build a new authorized API client service.        
//        Drive service = ServiceUtil.getDriveService();
//
////        uploadFile(service);
//        listFiles(service);
//        updateFile(service, FILE_ID, "Tickets", "application/vnd.google-apps.spreadsheet", "tickets.csv");
//    }
}
