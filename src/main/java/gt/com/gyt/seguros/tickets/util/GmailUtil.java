package gt.com.gyt.seguros.tickets.util;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Label;
import com.google.api.services.gmail.model.ListLabelsResponse;
import com.google.api.services.gmail.model.ListThreadsResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.Thread;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

public class GmailUtil {

    private static final String TICKET_NUMBER_PATTERN = "((2019|2020)[0-9]{12})";
    private static final String DATE_PATTERN = "dd/MM/yyyy HH:mm";
    private static final String QUERY = "after:2020/01/01 AND before:2020/02/05";
    private static final Map<String, String> LABELS = new HashMap<>();
    private static final List<String> EXCLUDED_LABELS = Arrays.asList("IMPORTANT",
            "CATEGORY_FORUMS",
            "CATEGORY_SOCIAL",
            "CATEGORY_PERSONAL",
            "CATEGORY_UPDATES",
            "CHAT",
            "TRASH",
            "INBOX",
            "SENT",
            "STARRED",
            "CATEGORY_PROMOTIONS",
            "UNREAD",
            "SPAM",
            "DRAFT"
    );

    private static final String[] HEADERS = {"Sujeto",
        "Número de ticket",
        "Número de respuestas",
        "Area",
        "Init Date",
        "First Response Date",
        "End Date",
        "Primer Resíestas",
        "Ultima Respuesta"};

//    public static void main(String... args) throws IOException, GeneralSecurityException {
//        Gmail service = ServiceUtil.getGmailService();
//        String user = "me";
//        getThreadsByQuery(service, user, QUERY);
//    }

    public static List<Label> getLabels(Gmail service, String user) throws IOException {
        ListLabelsResponse listResponse = service.users().labels().list(user).execute();
        List<Label> labels = listResponse.getLabels();
        if (labels.isEmpty()) {
            System.out.println("No labels found.");
        } else {
            System.out.println("Labels:");
            for (Label label : labels) {
                System.out.printf("- %s\n", label.getName() + " - " + label.getId());
                if (!EXCLUDED_LABELS.contains(label.getName())) {
                    LABELS.put(label.getId(), label.getName());
                }
            }
        }
        return labels;
    }

    /**
     *
     * @param messageId
     * @param service Authorized Gmail API instance.
     * @throws GeneralSecurityException
     */
    public static Map getMessageDetails(String messageId, Gmail service) throws GeneralSecurityException {
        Map<String, Object> messageDetails = new HashMap<>();
        try {

            Message message = service.users().messages().get("me", messageId).setFormat("raw").execute();

            byte[] emailBytes = Base64.decodeBase64(message.getRaw());
//            getHeaders(message);

            Properties props = new Properties();
            Session session = Session.getDefaultInstance(props, null);

            MimeMessage email = new MimeMessage(session, new ByteArrayInputStream(emailBytes));
            messageDetails.put("subject", email.getSubject());
            messageDetails.put("from", email.getSender() != null ? email.getSender().toString() : "None");
            messageDetails.put("time", email.getSentDate() != null ? email.getSentDate().toString() : "None");
            messageDetails.put("receivedDate", message.getInternalDate() != null ? new Date(message.getInternalDate()).toString() : "None");
            messageDetails.put("snippet", message.getSnippet());
            messageDetails.put("threadId", message.getThreadId());
            messageDetails.put("id", message.getId());
            messageDetails.put("sender", Arrays.toString(email.getFrom()));
//            THREADS.put(message.getThreadId(), Arrays.toString(email.getFrom()));
//            System.out.println("body " + getText(email));
//            messageDetails.put("body", getText(email));

        } catch (MessagingException | IOException ex) {
//            Logger.getLogger(GoogleAuthHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
//            Logger.getLogger(GoogleAuthHelper.class.getName()).log(Level.SEVERE, null, ex);
        return messageDetails;
    }

    public static Map<String, String> getMessageDetailsByThreadId(String threadId, Gmail service) throws GeneralSecurityException {
        Map<String, String> result = new HashMap();
        Map<String, String> lasMessageDetails = new HashMap();
        List<String> labels = new ArrayList<>();
        try {
            com.google.api.services.gmail.model.Thread thread = service.users().threads().get("me", threadId).execute();
            System.out.println("Thread id: " + thread.getId());
            System.out.println("No. of messages in this thread: " + thread.getMessages().size());
            Integer threadSize = thread.getMessages().size();
            List<Message> messages = thread.getMessages();
            if (messages != null) {
                result.putAll(getMessageDetails(messages.get(0).getId(), service));
                lasMessageDetails.putAll(getMessageDetails(messages.get(messages.size() - 1).getId(), service));
                for (String label : thread.getMessages().get(0).getLabelIds()) {
                    if (!EXCLUDED_LABELS.contains(label)) {
                        labels.add(LABELS.get(label));
                    }
                }
                result.put("labels", labels.toString());
                Date initDate = new Date(thread.getMessages().get(0).getInternalDate());
                Date firstResponseDate = null;
                if (thread.getMessages().size() >= 2) {
                    firstResponseDate = new Date(thread.getMessages().get(1).getInternalDate());
                }

                Date endDate = new Date(thread.getMessages().get(threadSize - 1).getInternalDate());
                result.put("threadsSize", threadSize.toString());
                result.put("initDate", dateToString(initDate));
                result.put("firstResponseDate", dateToString(firstResponseDate));
                result.put("endDate", dateToString(endDate));
                result.put("ticketNumber", getTicket(result.get("subject")));
                result.put("lastSender", lasMessageDetails.get("sender"));
            }
        } catch (IOException ex) {
            Logger.getLogger(GmailUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public static String getTicket(String subject) {
        String result = null;
        if (subject != null && !subject.isEmpty()) {
            Pattern pattern = Pattern.compile(TICKET_NUMBER_PATTERN);
            Matcher matcher = pattern.matcher(subject);
            if (matcher.find()) {
                result = matcher.group(1);
            }
        }
        return result;
    }

    public static String dateToString(Date date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
        return sdf.format(date);
    }

    /**
     * List all Threads of the user's mailbox matching the query.
     *
     * @param service Authorized Gmail API instance.
     * @param userId User's email address. The special value "me" can be used to
     * indicate the authenticated user.
     * @param query String used to filter the Threads listed.
     * @throws IOException
     */
    public static List<Thread> getThreadsByQuery(Gmail service, String userId,
            String query) throws IOException {
        getLabels(service, userId);
//        List<Map<String, String>> result = new ArrayList<>();
        ListThreadsResponse response = service.users().threads().list(userId).setQ(query).execute();
        List<Thread> threads = new ArrayList<Thread>();

        while (response.getThreads() != null) {
            threads.addAll(response.getThreads());
            if (response.getNextPageToken() != null) {
                String pageToken = response.getNextPageToken();
                response = service.users().threads().list(userId).setQ(query).setPageToken(pageToken).execute();
            } else {
                break;
            }
        }
        System.out.println("threads number " + threads.size());
        return threads;
//        for (Thread thread : threads) {
//            Map<String, String> map = new HashMap<>();
//            try {
//
//                map.putAll(getMessageDetailsByThreadId(thread.getId(), service));
////                System.out.println(result);
////            System.out.println(thread.toPrettyString());
//            } catch (GeneralSecurityException ex) {
//                Logger.getLogger(GmailUtil.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            result.add(map);
//        }
//        CsvUtil.createCSVFile(result);
    }

}
