/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.com.gyt.seguros.tickets.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

/**
 *
 * @author descolin
 */
public class CsvUtil {

    private static final String[] HEADERS = {"Sujeto",
        "Numero de ticket",
        "Cantidad de respuestas",
        "Area",
        "Fecha Recibido",
//        "Fecha Respuesta",
        "Fecha Cierre",
        "Primer Respuesta",
        "Ultima Respuesta"};

    public static void createCSVFile(List<Map<String, String>> result) throws IOException {
        PrintWriter out = new PrintWriter("tickets.csv", "UTF-8");
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT
                .withHeader(HEADERS))) {
            System.out.println("No. of messages " + result.size());
            for (Map<String, String> map : result) {
                String ticketNumber = map.get("ticketNumber");
                String labels = map.get("labels");
                if ((ticketNumber == null || ticketNumber.isEmpty())) {
                    continue;
                }
                if (labels.contains("T.Cerrado")) {
                    continue;
                }
                printer.printRecord(map.get("subject"),
                        map.get("ticketNumber"),
                        map.get("threadsSize"),
                        map.get("labels"),
                        map.get("initDate"),
//                        map.get("firstResponseDate"),
                        map.get("endDate"),
                        map.get("sender"),
                        map.get("lastSender"));
            }
        }
    }
}
